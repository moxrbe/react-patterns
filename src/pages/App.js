import "../styles/App.scss";
import LoginContent from "./Login/Login.content.js";
import LoginForm from "./Login/Login.form.js";

export default function App() {
  return (
    <div className="login">
      <LoginContent />
      <LoginForm />
    </div>
  );
}
