import Form from "../../components/Form/Form";
import Contact from "../Contact/Contact";
import styles from "./login.module.scss";

export default function LoginForm() {
  const contactInfo = {
    phone: "+998 (90) 123-45-67",
    text: "Служба поддержки",
    icon: "../../../images/phone.svg",
  };
  return (
    <>
      <div className={styles.login_form}>
        <Form />
        <Contact data={contactInfo} />
      </div>
    </>
  );
}
