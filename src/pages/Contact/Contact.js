import styles from "./contact.module.scss";

export default function Contact({ data }) {
  return (
    <>
      <div className={styles.contact}>
        <div className={styles.contact_info}>
          <img alt="phone" src={data.icon} />
          <p className={styles.contact_txt}>{data.text}</p>
        </div>
        <h3 className={styles.contact_phone}>{data.phone}</h3>
      </div>
    </>
  );
}
