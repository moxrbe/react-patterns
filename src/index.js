import React from "react";
import ReactDOM from "react-dom";
import "./styles/global.css";
import App from "./pages/App.js";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
