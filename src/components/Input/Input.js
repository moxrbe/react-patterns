import styles from "./input.module.scss";

export default function Input({ data }) {
  return (
    <>
      <div className={styles.input_block}>
        {data.map((item) => (
          <div className={styles.input_body}>
            <p className={styles.input_txt}>{item.text}</p>
            <input
              placeholder={`${item.text}`}
              type="text"
              className={styles.input}
            />
          </div>
        ))}
      </div>
    </>
  );
}
