import styles from "./button.module.scss";

export default function Button({ data }) {
  return (
    <>
      <div className={styles.button_block}>
        <button type="button" className={styles.button}>
          {data.text}
        </button>
      </div>
    </>
  );
}
