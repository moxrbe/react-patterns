import Button from "../Button/button";
import Input from "../Input/Input";
import styles from "./form.module.scss";

export default function Form() {
  const placeholders = [
    {
      text: "Имя пользователья",
      icon: "&#xf007",
    },
    {
      text: "Пароль",
      icon: "../../../public/images/lock.svg",
    },
  ];
  const button = {
    text: "Войти",
  };
  return (
    <>
      <div className={styles.form}>
        <div className={styles.form_header}>
          <h3 className={styles.form_txt}>Войти в систему</h3>
        </div>
        <div className={styles.form_body}>
          <Input data={placeholders} />
          <Button data={button} />
        </div>
      </div>
    </>
  );
}
